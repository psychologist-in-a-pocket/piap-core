package de.winteger.piap.evaluation;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import de.winteger.piap.R;
import de.winteger.piap.core.GlobalSettings;
import de.winteger.piap.core.InputLog;
import de.winteger.piap.core.InputLogComperator;
import de.winteger.piap.data.DataSource;
import de.winteger.piap.evaluation.KeywordsDataSource.Category;
import de.winteger.piap.guicontent.CManager;
import de.winteger.piap.helper.DeviceInfo;
import de.winteger.piap.helper.TimeHelper;

/**
 * This service evaluates the logged data
 * 
 * @author sarah
 * 
 */
public class EvaluationService extends Service {

	private final String TAG = "LoggerEvalService";

	private final IBinder myBinder = new MyBinder(); // can be used for communication with the service, is currently not used
	private KeywordsDataSource source; // keyword access object
	private DataSource datasource; // database access object
	private ArrayList<InputLog> resultListAll; // used to store the logs with keywords
	private Evaluation evaluation; // used to store the evaluation results

	/**
	 * is called when the service is started via startService() by
	 * MyStartServiceReceiver
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i(TAG, "Started via onStartCommand");

		if (CManager.CTX == null){
			CManager.CTX = this;
		}

		// Do the evaluation according to the received intent
		int mode = intent.getIntExtra(GlobalSettings.EVAL_MODE, GlobalSettings.EVAL_MODE_USER);

		switch (mode) {
		case GlobalSettings.EVAL_MODE_SCHEDULE:
			// enable fast scheduling for demo purposes
			if (GlobalSettings.isDemo()) {
				break;
			}

			// Check global settings
			boolean onlyWhenCharging = GlobalSettings.getChargingSetting();
			if (onlyWhenCharging == true) {
				// check if device is charging
				if (DeviceInfo.hasExternalPower(this)) {
					// charging
					Log.i(TAG, "Device is charging, start evaluation");
				} else {
					// not charing, do not evaluate return
					Log.i(TAG, "Device is not charging, don't start evaluation");
					return Service.START_NOT_STICKY;

				}
			} else {
				// evaluate anyway
			}
			break;
		// for all other settings evaluate
		case GlobalSettings.EVAL_MODE_ON_AC_CONNECT:
			if (!evalNeeded()) {
				return Service.START_NOT_STICKY;
			}
			break;
		case GlobalSettings.EVAL_MODE_USER:
		default:
			break;
		}

		shortToast(getString(R.string.computing_evaluation));

		// try to load keywords source
		source = loadKeywordSource();
		if (source == null) {
			Log.i(TAG, "loading source returned null, try to genrate source");
			// some error occurred
			// generate empty source
			source = new KeywordsDataSource();
			// fill source with keywords
			genrateArraysFromFile(source);
			// save source
			saveKeywordSource();
		}

		// new evaluation object
		evaluation = new Evaluation(this);

		// store logs with keywords from the last 2 weeks and some statistics in
		// the evaluation object
		getRelevantData();

		// evaluate
		computeEvaluation(evaluation);

		if (GlobalSettings.getNotifySetting() && checkIfSmsUpdateIsNeeded(evaluation)) {

			/*
			 * TODO: NOTE: move this code into the plugin, as we have no control
			 * over if the sms has been sent correctly or not.
			 */

			if (GlobalSettings.getLastSMSTimestamp() == GlobalSettings.LAST_SMS_TIMESTAMP_UNSET) {
				// send sms if no sms sent before
				GlobalSettings.sendSMSNotification(getString(R.string.msg_sms_default_notification));
				// set when last sms was sent
				GlobalSettings.setLastSMSTimestamp(System.currentTimeMillis());
			} else if (GlobalSettings.getSMSMinInterval() < System.currentTimeMillis() - GlobalSettings.getLastSMSTimestamp()) {
				// send sms if last sms is old enough
				GlobalSettings.sendSMSNotification(getString(R.string.msg_sms_default_notification));
				// set when last sms was sent
				GlobalSettings.setLastSMSTimestamp(System.currentTimeMillis());
			}
		}

		GlobalSettings.setEvaluationResult(evaluation.getEvalResult());

		// save evaluation
		saveEvaluation();

		GlobalSettings.setLastEvalTimestamp(System.currentTimeMillis());

		shortToast(getString(R.string.computing_evaluation_finished));

		// this service is not restarted automatically if it is terminated
		return Service.START_NOT_STICKY;
	}

	private boolean evalNeeded() {
		long lastTS = GlobalSettings.getLastEvalTimestamp();
		
		// automatic eval = never
		if (GlobalSettings.getIntervalHours() == 0) {
			return false;
		}

		// first eval ever
		if (lastTS == GlobalSettings.LAST_EVAL_TIMESTAMP_UNSET) {
			return true;
		}

		if (GlobalSettings.getIntervalHours() * TimeHelper.INTERVAL_HOUR < System.currentTimeMillis() - lastTS) {
			return true;
		}

		return false;
	}

	private boolean checkIfSmsUpdateIsNeeded(Evaluation eval) {
		if (eval.getEvalResult() <= Evaluation.EVAL_NOT_DEPRESSED) {
			// nothing to do here...
			return false;
		}

		return true;
	}

	/**
	 * Computes the result for the gathered data in the evaluation object
	 * 
	 * @param evaluation
	 */
	private void computeEvaluation(Evaluation evaluation) {
		// check for keywords in 5 categories
		// category DEPRESSED_MOOD and LOSS_OF_INTEREST must occur
		// over a timespan of 2 weeks

		boolean demo = GlobalSettings.isDemo();

		if (demo) {
			// check if logs with keywords span 3 minutes.
			if (evaluation.getTimespanEnd() - evaluation.getTimespanBegin() < 180000) {
				// timespan is less
				evaluation.setInfo(getString(R.string.you_do_not_seem_depressed));
				evaluation.setEvalResult(Evaluation.EVAL_NOT_DEPRESSED);
			} else {
				// timespan is okay, at least 5 keywords
				if (evaluation.getNumberOfkeywords() >= 5) {
					evaluation.setInfo(getString(R.string.you_may_be_depressed));
					evaluation.setEvalResult(Evaluation.EVAL_MAY_BE_DEPRESSED);
				} else {
					evaluation.setInfo(getString(R.string.you_do_not_seem_depressed));
					evaluation.setEvalResult(Evaluation.EVAL_NOT_DEPRESSED);
				}
			}
		} else {

			// check if logs with keywords span two weeks.
			if (evaluation.getTimespanEnd() - evaluation.getTimespanBegin() < TimeHelper.INTERVAL_WEEK * 2) {
				// timespan is less than 2 weeks
				evaluation.setInfo(getString(R.string.you_do_not_seem_depressed));
				evaluation.setEvalResult(Evaluation.EVAL_NOT_DEPRESSED);
			} else {
				// timespan is 2 weeks
				if (evaluation.getNumberOfCategories() >= 5 && evaluation.getCategoryStatus(Category.DEPRESSED_MOOD.getValue())
						&& evaluation.getCategoryStatus(Category.LOSS_OF_INTEREST.getValue())) {
					evaluation.setInfo(getString(R.string.you_may_be_depressed));
					evaluation.setEvalResult(Evaluation.EVAL_MAY_BE_DEPRESSED);
				} else {
					evaluation.setInfo(getString(R.string.you_do_not_seem_depressed));
					evaluation.setEvalResult(Evaluation.EVAL_NOT_DEPRESSED);
				}
			}
		}
	}

	/**
	 * Saves the evaluation
	 * 
	 * @return 0 if save was successful, -1 else
	 */
	private int saveEvaluation() {
		try {
			FileOutputStream fos = openFileOutput("lastEval.eval", Context.MODE_PRIVATE);
			ObjectOutputStream obj_out = new ObjectOutputStream(fos);
			obj_out.writeObject(evaluation);
			fos.close();
			Log.d(TAG, "Saved evaluation");
		} catch (FileNotFoundException e) {
			Log.d(TAG, "Save evaluation fail, filenotfound " + e.getMessage());
			return -1;
		} catch (IOException e) {
			Log.d(TAG, "Save evaluation fail " + e.getMessage());
			return -1;
		}
		return 0;
	}

	/**
	 * searches the DB for logs with keywords, stores the logs in resultListAll
	 * updates infos about found categories in the evaluation object
	 */
	private void getRelevantData() {
		// temporary list that stores the logs with a specific keyword
		ArrayList<InputLog> resultList = new ArrayList<InputLog>();
		// stores all logs with keywords
		resultListAll = new ArrayList<InputLog>();

		// open DB
		datasource = new DataSource(this);
		datasource.openRead();

		if (source == null) {
			Log.d(TAG, "source null");
		} else { // if the keyword source is available
			boolean once = true;
			// for each category
			for (KeywordArray keywordArray : source.getKeywordsArrays()) {
				if (keywordArray != null) {
					once = true;
					// check for each keyword in this category
					for (String keyword : keywordArray.keywords) {
						// get logs with keyword from the last two weeks
						datasource.getAllLogEntriesWithKeyword(resultList, keyword, TimeHelper.getTimestampTwoWeeksAgo(), System.currentTimeMillis());
						// update statistics in evaluation
						if (!resultList.isEmpty()) {
							evaluation.setCategoryStatus(keywordArray.category);
							if (once) {
								evaluation.incrementNumberOfCategories();
								once = false;
							}
						}
						// append logs to overall result list
						setCategoryStatusAll(keywordArray.category, resultList);
						fuseLists(resultListAll, resultList, keywordArray.category);
					}
				}
			}
		}

		// close DB
		datasource.close();

		// sort result list by time
		Collections.sort(resultListAll, new InputLogComperator());

		// update evaluation
		evaluation.setRelevantLogs(resultListAll);
		evaluation.computeNumberOfKeywords();
		// if logs with keywords exist, set first and last occurrence
		if (resultListAll.size() > 0) {
			evaluation.setTimespanBegin(resultListAll.get(0).getTimestamp());
			evaluation.setTimespanEnd(resultListAll.get(resultListAll.size() - 1).getTimestamp());
		}
	}

	/**
	 * Sets flag for the category category to true in every element of the list
	 * list
	 * 
	 * @param category
	 *            The category
	 * @param list
	 *            The list
	 */
	private void setCategoryStatusAll(int category, ArrayList<InputLog> list) {
		for (InputLog log : list) {
			log.setCategoryStatus(category);
		}
	}

	/**
	 * Adds all items from the list addList to the list resultList, if
	 * resultList already contains the element the flag for the new category is
	 * set, numberOfKeywords, and NumberOfCategories is updated
	 * 
	 * @param resultList
	 *            The list that the elements are added to
	 * @param addList
	 *            The list which elements should be added
	 * @param category
	 *            The category
	 */
	private void fuseLists(ArrayList<InputLog> resultList, ArrayList<InputLog> addList, int category) {
		if (resultList != null && addList != null) {
			// check for each item in addList...
			for (InputLog log : addList) {
				if (!resultList.contains(log)) {
					// log is completely new
					// set statistic values to 1
					log.setNumberCategories(1);
					log.setNumberKeywords(1);
					log.incrementCategoryCounter(category);
					resultList.add(log);
				} else {
					// log was already found
					// get the log and update the info
					int index = resultList.indexOf(log);
					InputLog resultLog = resultList.get(index);

					// found 1 more keyword
					resultLog.setNumberKeywords(resultLog.getNumberKeywords() + 1);
					resultLog.incrementCategoryCounter(category);
					// check if a new category occurred
					if (!resultLog.getCategoryStatus(category)) {
						// keyword belongs to a new category
						resultLog.setCategoryStatus(category);
						resultLog.setNumberCategories(resultLog.getNumberCategories() + 1);
					}

				}
			}
		}
	}

	/**
	 * is called when a application binds to the service
	 */
	@Override
	public IBinder onBind(Intent arg0) {
		return myBinder;
	}

	public class MyBinder extends Binder {
		public EvaluationService getService() {
			return EvaluationService.this;
		}
	}

	/**
	 * Displays a short toast with message msg
	 * 
	 * @param msg
	 */
	private void shortToast(String msg) {

		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(this, msg, duration);
		toast.show();
	}

	/**
	 * loads the keywords source
	 * 
	 * @return the keywords source
	 */
	private KeywordsDataSource loadKeywordSource() {
		String filePath = "keywordsDB.source";
		FileInputStream fis;
		KeywordsDataSource temp = null;
		Log.i(TAG, "Try to load keywords source");
		try {
			fis = openFileInput(filePath);
			ObjectInputStream obj_in = new ObjectInputStream(fis);
			temp = (KeywordsDataSource) obj_in.readObject();
			fis.close();
		} catch (FileNotFoundException e) {
			Log.i(TAG, "file not found " + e.getMessage());
			return null;
		} catch (StreamCorruptedException e) {
			Log.i(TAG, "stream corrupted " + e.getMessage());
			return null;
		} catch (IOException e) {
			Log.i(TAG, "ioexption " + e.getMessage());
			return null;
		} catch (ClassNotFoundException e) {
			Log.i(TAG, "class not found " + e.getMessage());
			return null;
		}
		Log.i(TAG, "Loaded keywordsource");
		return temp;
	}

	/**
	 * 
	 * 
	 * @return 0 if save was successful, -1 else
	 */
	private int saveKeywordSource() {
		try {
			FileOutputStream fos = openFileOutput("keywordsDB.source", Context.MODE_PRIVATE);
			ObjectOutputStream obj_out = new ObjectOutputStream(fos);
			obj_out.writeObject(source);
			fos.close();
			Log.i(TAG, "Saved keyword source");
		} catch (FileNotFoundException e) {
			Log.i(TAG, "Save keyword source fail, filenotfound " + e.getMessage());
			return -1;
		} catch (IOException e) {
			Log.i(TAG, "Save keyword source fail " + e.getMessage());
			return -1;
		}
		return 0;
	}

	/**
	 * Generates the keywords source from the file provided in the assests
	 * 
	 * @param source
	 *            the keywords source
	 */
	private void genrateArraysFromFile(KeywordsDataSource source) {
		try {
			// open the keywords text file
			InputStream in = this.getApplicationContext().getAssets().open("keywords.source");
			BufferedReader buf = new BufferedReader(new InputStreamReader(in, Charset.defaultCharset()));
			String s = "";
			// init the arrays
			ArrayList<KeywordArray> arrays = source.addKeywordArraysToList();
			KeywordArray a = null;
			// for every line add keyword to the specific array
			while ((s = buf.readLine()) != null) {
				// Log.i(TAG, s);
				String[] split = s.split(";");
				int category = Integer.parseInt(split[0]);
				int categoryPosInArray = category - 1;
				String keyword = split[1];
				// check if array for keyword exists, if not crate it
				if ((a = arrays.get(categoryPosInArray)) == null) {
					a = new KeywordArray(category);
					// don't forget to set the reference
					arrays.set(categoryPosInArray, a);
				}
				// add the keyword
				a.addKeyWord(keyword);
			}
			// close the text file
			buf.close();
			// don't forget to set the reference
			source.setKeywordsArrays(arrays);
		} catch (IOException e) {
			Log.i(TAG, "Could not generate keyword source from file " + e.getMessage());
		}
	}

}
