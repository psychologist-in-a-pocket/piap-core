package de.winteger.piap.evaluation;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import de.winteger.piap.core.GlobalSettings;
import de.winteger.piap.guicontent.CManager;

/**
 * (Re)starts the services after (re-)boot, used to set up the interval
 * schedule, (re-)create the proximity alert for location logging
 * 
 * @author sarah
 * 
 */
public class EvaluationScheduleManager extends BroadcastReceiver {

	/**
	 * is automatically called when ACTION_BOOT_COMPLETED or
	 * ACTION_POWER_CONNECT broadcast is received
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		
		CManager.CTX = context;
		
		if (intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {
			// start evaluation Service
			Intent service = new Intent(context, EvaluationService.class);
			service.putExtra(GlobalSettings.EVAL_MODE, GlobalSettings.EVAL_MODE_ON_AC_CONNECT);
			context.startService(service);
		}
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			// set up automatically evaluation interval
			updateSchedule(context);
			// recreate proximity alert for location logging
			boolean locationLogging = GlobalSettings.getLocationLoggingSetting();
			if (locationLogging) {
				GlobalSettings.updateLocationLogging(context, true);
			}
		}
	}

	/**
	 * Use this method to update the Schedule of the Evaluation, the interval is
	 * read out from the global settings
	 * 
	 * @param context
	 */
	public static void updateSchedule(Context context) {
		AlarmManager service = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, EvaluationServiceStarter.class);
		PendingIntent pending = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
		// load settings
		int hours = GlobalSettings.getIntervalHours();
		long repeatInterval = AlarmManager.INTERVAL_HOUR * hours;
		Calendar rightNow = Calendar.getInstance();

		boolean demo = GlobalSettings.isDemo();
		if (demo) {
			rightNow.add(Calendar.SECOND, 5);
			// repeat every 30 seconds
			service.setRepeating(AlarmManager.RTC_WAKEUP, rightNow.getTimeInMillis(), 30000, pending);
		} else {
			// Start repeatInterval hours after this method is called
			rightNow.add(Calendar.HOUR_OF_DAY, hours);
			//rightNow.add(Calendar.SECOND, 10);
			// set the schedule via alarm manager
			if (repeatInterval == 0) {
				// cancel automatic starting of the service
				service.cancel(pending);
			} else {
				// Start Service every repeatInterval hours
				// setInexactRepeating() allows Android to optimize the energy consumption but might not start the service the first time :(
				// so use setRepeating()....
				service.setRepeating(AlarmManager.RTC_WAKEUP, rightNow.getTimeInMillis(), repeatInterval, pending);
			}
		}
	}
}