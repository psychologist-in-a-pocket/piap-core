package de.winteger.piap.data;

import java.util.Comparator;
import java.util.Date;

/**
 * Date Comparator
 * 
 * @author sarah
 * 
 */
public class DateComperator implements Comparator<Date> {

	/**
	 * compares log timestamps, lhs < rhs iff lhs.timestamp < rhs.timestamp
	 */
	public int compare(Date l0, Date l1) {
		// an integer < 0 if lhs is less than rhs
		// 0 if they are equal
		// and > 0 if lhs is greater than rhs.
		return Double.compare(l0.getTime(), l1.getTime());
	}
}