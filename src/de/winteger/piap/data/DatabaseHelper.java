package de.winteger.piap.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * This class takes care of opening the database if it exists, creating it if it does not, and upgrading it as necessary
 * @author sarah
 *
 */
public class DatabaseHelper extends SQLiteOpenHelper {
	
	public static final String TAG = "LoggerDB";

	// Define database, table and column names
	
	// Table for input logging
	public static final String TABLE_LOGS = "table_logs";
	public static final String T0_C0_TIMESTAMP = "_id";
	public static final String T0_C1_APP_NAME = "app_name";
	public static final String T0_C2_APP_PKG = "app_pkg";
	public static final String T0_C3_INPUT = "input";
	
	// Table for apps that should be logged
	public static final String TABLE_WHITELIST = "table_whitelist";
	public static final String T1_C0_APP_NAME = "app_name";
	public static final String T1_C1_APP_PKG = "app_pkg";
	
	// Table for location logging
	public static final String TABLE_LOCATION = "table_location";
	public static final String T2_C0_TIMESTAMP = "_id";
	public static final String T2_C1_ENTERING = "entering"; //stored as integer because sqlite has no bool

	private static final String DATABASE_NAME = "logs.db";
	
	// Current DB version, update if needed
	private static final int DATABASE_VERSION = 13;

	// SQLite statements to create the tables
	static final String DATABASE_CREATE_T0 = "create table " + TABLE_LOGS + "(" + T0_C0_TIMESTAMP + " long primary key, " + T0_C1_APP_NAME + " text not null, "
			+ T0_C2_APP_PKG + " text not null, " + T0_C3_INPUT + " text not null);";

	static final String DATABASE_CREATE_T1 = "create table " + TABLE_WHITELIST + "(" + T1_C0_APP_NAME + " text not null, " + T1_C1_APP_PKG + " text not null);";
	
	static final String DATABASE_CREATE_T2 = "create table " + TABLE_LOCATION + "(" + T2_C0_TIMESTAMP + " long primary key, " + T2_C1_ENTERING + " integer not null);";
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE_T0);
		database.execSQL(DATABASE_CREATE_T1);
		database.execSQL(DATABASE_CREATE_T2);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.i(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOGS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_WHITELIST);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
		onCreate(db);
	}

}
