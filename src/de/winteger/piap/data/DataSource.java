package de.winteger.piap.data;

import java.util.ArrayList;

import de.winteger.piap.core.AppListItem;
import de.winteger.piap.core.InputLog;
import de.winteger.piap.core.LocationLog;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * This class is a DAO: Database Access Object
 * 
 * @author sarah
 * 
 */
public class DataSource {

	public static final String TAG = "LoggerDB";

	// Database fields
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	private String[] allColumnsT0 = { DatabaseHelper.T0_C0_TIMESTAMP, DatabaseHelper.T0_C1_APP_NAME, DatabaseHelper.T0_C2_APP_PKG, DatabaseHelper.T0_C3_INPUT };
	private String[] allColumnsT1 = { DatabaseHelper.T1_C0_APP_NAME, DatabaseHelper.T1_C1_APP_PKG };
	private String[] allColumnsT2 = { DatabaseHelper.T2_C0_TIMESTAMP, DatabaseHelper.T2_C1_ENTERING };

	public DataSource(Context context) {
		dbHelper = new DatabaseHelper(context);
	}

	/**
	 * opens the DB with write access
	 */
	public void openWrite() {
		database = dbHelper.getWritableDatabase();
	}

	/**
	 * opens the DB with read-only access
	 */
	public void openRead() {
		database = dbHelper.getReadableDatabase();
	}

	/**
	 * closes the DB
	 */
	public void close() {
		dbHelper.close();
	}

	/**
	 * Convenience method for deleting all input logs from the log table.
	 */
	public boolean deleteAllLogs() {
		int doneDelete = 0;
		doneDelete = database.delete(DatabaseHelper.TABLE_LOGS, null, null);
		Log.w(TAG, Integer.toString(doneDelete));
		return doneDelete > 0;
	}

	/**
	 * inserts a new input log with the given parameters into the DB
	 * 
	 * @param timestamp
	 * @param appName
	 * @param appPkg
	 * @param input
	 * @return
	 */
	public long createLog(long timestamp, String appName, String appPkg, String input) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.T0_C0_TIMESTAMP, timestamp);
		values.put(DatabaseHelper.T0_C1_APP_NAME, appName);
		values.put(DatabaseHelper.T0_C2_APP_PKG, appPkg);
		values.put(DatabaseHelper.T0_C3_INPUT, input);
		long insertID = database.insert(DatabaseHelper.TABLE_LOGS, null, values);
		return insertID;
	}

	/**
	 * deletes the given input log from the DB if it exists
	 * 
	 * @param log
	 */
	public void deleteLog(InputLog log) {
		long id = log.getTimestamp();
		Log.i(TAG, "Comment deleted with id: " + id);
		database.delete(DatabaseHelper.TABLE_LOGS, DatabaseHelper.T0_C0_TIMESTAMP + " = " + id, null);
	}

	/**
	 * Converts a DB entry from the log table to an InputLog log
	 * 
	 * @param cursor
	 *            the entry
	 * @return the log
	 */
	private InputLog cursorToLog(Cursor cursor) {
		InputLog log = new InputLog();
		log.setTimestamp(cursor.getLong(0));
		log.setAppName(cursor.getString(1));
		log.setAppPkg(cursor.getString(2));
		log.setInput(cursor.getString(3));
		return log;
	}

	/**
	 * Converts all entries from the log table to an ArrayList of InputLogs
	 * 
	 * @param dbEntryList
	 *            ArrayList that stores the result
	 */
	public void getAllLogEntries(ArrayList<InputLog> dbEntryList) {
		Log.i(TAG, "DB READ START");
		dbEntryList.clear();

		// Retrieve all input logs
		Cursor cursor = database.query(DatabaseHelper.TABLE_LOGS, allColumnsT0, null, null, null, null, null);

		// convert all rows to InputLogs and add them to the result list
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			InputLog log = cursorToLog(cursor);
			dbEntryList.add(log);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		Log.i(TAG, "DB READ END");

	}

	/**
	 * Converts all entries that contain the keyword and are in the given
	 * timespan from the log table to an ArrayList of InputLogs
	 * 
	 * @param dbEntryList
	 *            ArrayList that stores the result
	 * @param keyword
	 *            the keyword
	 * @param timespanBegin
	 *            the oldest timestamp
	 * @param timespanEnd
	 *            the newest timestamp
	 */
	public void getAllLogEntriesWithKeyword(ArrayList<InputLog> dbEntryList, String keyword, long timespanBegin, long timespanEnd) {
		// Log.i(TAG, "DB READ START");
		dbEntryList.clear();

		// writing LIKE %?% only gives errors :(
		// Retrieve all logs with the keyword
		String SELECTION = DatabaseHelper.T0_C3_INPUT + " LIKE ? AND " + DatabaseHelper.T0_C0_TIMESTAMP + " >= ? AND " + DatabaseHelper.T0_C0_TIMESTAMP
				+ " <=?";
		String[] SELECTION_ARGS = new String[] { "%" + keyword + "%", "" + timespanBegin, "" + timespanEnd };
		Cursor cursor = database.query(DatabaseHelper.TABLE_LOGS, allColumnsT0, SELECTION, SELECTION_ARGS, null, null, null);

		// convert result to InputLogs and add them to the array list
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			InputLog log = cursorToLog(cursor);
			dbEntryList.add(log);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		// Log.i(TAG, "DB READ END");

	}

	/**
	 * inserts a new application with the given parameters into the DB
	 * 
	 * @param appName
	 * @param appPkg
	 * @return
	 */
	public long createWhiteListItem(String appName, String appPkg) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.T1_C0_APP_NAME, appName);
		values.put(DatabaseHelper.T1_C1_APP_PKG, appPkg);
		long insertID = database.insert(DatabaseHelper.TABLE_WHITELIST, null, values);
		return insertID;
	}

	/**
	 * deletes the given application from the DB if it exists
	 * 
	 * @param application
	 */
	public void deleteWhiteListItem(AppListItem item) {
		String name = item.getAppName();
		String pkg = item.getAppPkg();
		Log.i(TAG, "Whitelist item deleted with name " + name + "and pkg " + pkg);
		database.delete(DatabaseHelper.TABLE_WHITELIST, DatabaseHelper.T1_C0_APP_NAME + " = '" + name + "' AND " + DatabaseHelper.T1_C1_APP_PKG + " = '" + pkg
				+ "'", null);
	}

	/**
	 * Converts a DB entry from the application table to an AppListItem app
	 * 
	 * @param cursor
	 *            the entry
	 * @return the app
	 */
	private AppListItem cursorToWhiteListItem(Cursor cursor) {
		AppListItem item = new AppListItem();
		item.setAppName(cursor.getString(0));
		item.setAppPkg(cursor.getString(1));
		return item;
	}

	/**
	 * Converts all entries from the application table to an ArrayList of
	 * AppListItems
	 * 
	 * @param dbEntryList
	 *            ArrayList that stores the result
	 */
	public void getAllWhiteListItemEntries(ArrayList<AppListItem> dbEntryList) {
		Log.i(TAG, "DB READ START");
		dbEntryList.clear();

		// Retrieve all application entries
		Cursor cursor = database.query(DatabaseHelper.TABLE_WHITELIST, allColumnsT1, null, null, null, null, null);

		// Convert all entries to AppListItems and add them to the result list
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			AppListItem item = cursorToWhiteListItem(cursor);
			dbEntryList.add(item);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		Log.i(TAG, "DB READ END");

	}

	/**
	 * inserts a new location entry with the given parameters to the DB
	 * 
	 * @param timestamp
	 *            The timestamp
	 * @param entering
	 *            The value, entering true if zone was entered
	 * @return
	 */
	public long createLocationEntry(long timestamp, boolean entering) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.T2_C0_TIMESTAMP, timestamp);
		values.put(DatabaseHelper.T2_C1_ENTERING, entering);
		long insertID = database.insert(DatabaseHelper.TABLE_LOCATION, null, values);
		return insertID;
	}
	
	/**
	 * Convenience method for deleting all location logs from the location log table.
	 */
	public boolean deleteAllLocationLogs() {
		int doneDelete = 0;
		doneDelete = database.delete(DatabaseHelper.TABLE_LOCATION, null, null);
		Log.w(TAG, Integer.toString(doneDelete));
		return doneDelete > 0;
	}

	/**
	 * deletes the given location entry from the DB if it exists
	 * 
	 * @param log
	 *            The entry
	 */
	public void deleteLocationEntry(LocationLog log) {
		long id = log.getTimestamp();
		Log.i(TAG, "Location deleted with id: " + id);
		database.delete(DatabaseHelper.TABLE_LOCATION, DatabaseHelper.T2_C0_TIMESTAMP + " = " + id, null);
	}

	/**
	 * Converts a DB entry from the location table to an LocationLog log
	 * 
	 * @param cursor
	 *            The entry
	 * @return The log
	 */
	private LocationLog cursorToLocationEntry(Cursor cursor) {
		LocationLog log = new LocationLog();
		log.setTimestamp(cursor.getLong(0));
		log.setEntering(cursor.getInt(1) == 0 ? false : true);
		return log;
	}

	/**
	 * Converts all location entries in the given timespan from the location
	 * table to an ArrayList of LocationLogs
	 * 
	 * @param dbEntryList
	 *            ArrayList that stores the result
	 * @param timespanBegin
	 *            the oldest timestamp
	 * @param timespanEnd
	 *            the newest timestamp
	 */
	public void getAllLocationEntries(ArrayList<LocationLog> dbEntryList, long timespanBegin, long timespanEnd) {
		// Log.i(TAG, "DB READ START");
		dbEntryList.clear();

		String SELECTION = DatabaseHelper.T2_C0_TIMESTAMP + " >= ? AND " + DatabaseHelper.T2_C0_TIMESTAMP + " <=?";
		String[] SELECTION_ARGS = new String[] { "" + timespanBegin, "" + timespanEnd };

		Log.i(TAG, "DB null = ? " + (database == null));

		Cursor cursor = database.query(DatabaseHelper.TABLE_LOCATION, allColumnsT2, SELECTION, SELECTION_ARGS, null, null, null);

		// convert result to LocationLogs and add them to the array list
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			LocationLog log = cursorToLocationEntry(cursor);
			dbEntryList.add(log);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		// Log.i(TAG, "DB READ END");
	}

	/**
	 * Returns true if logging for the app with the given parameters is enabled
	 * 
	 * @param pkgName
	 *            the application name
	 * @param appName
	 *            the application package
	 * @return
	 */
	public boolean isLoggingEnabled(String pkgName, String appName) {
		Log.i(TAG, "Checking: " + pkgName + "/" + appName);

		// Query the DB for the given application
		Cursor cursor = database.query(DatabaseHelper.TABLE_WHITELIST, allColumnsT1, DatabaseHelper.T1_C0_APP_NAME + " =? AND " + DatabaseHelper.T1_C1_APP_PKG
				+ " =?", new String[] { appName, pkgName }, null, null, null);
		// store the number of results
		int resultLen = cursor.getCount();
		// Make sure to close the cursor
		cursor.close();

		// if the result is not 0, the logging for the app is enabled
		if (resultLen >= 1) {
			return true;
		}
		// logging is not enabled
		return false;
	}
}
