package de.winteger.piap.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import de.winteger.piap.R;
import de.winteger.piap.core.EvalAccess;
import de.winteger.piap.core.GlobalSettings;
import de.winteger.piap.core.LocationLog;
import de.winteger.piap.evaluation.Evaluation;
import de.winteger.piap.proto.Messages.MoodLog;
import de.winteger.piap.proto.Messages.RootMessage;

public class DataConversion {

	public RootMessage getProtoLocationLogObjects(Context context) {
		RootMessage.Builder b = RootMessage.newBuilder();
		DataSource ds = new DataSource(context);
		ds.openRead();

		ArrayList<LocationLog> dbEntryList = new ArrayList<LocationLog>();
		ds.getAllLocationEntries(dbEntryList, 0, System.currentTimeMillis());

		for (de.winteger.piap.core.LocationLog lo : dbEntryList) {
			de.winteger.piap.proto.Messages.LocationLog.Builder log = de.winteger.piap.proto.Messages.LocationLog.newBuilder();

			log.setEntering(lo.isEntering());
			log.setTimestamp(lo.getTimestamp());

			b.addLocationLog(log.build());
		}
		ds.close();
		return b.build();
	}

	/**
	 * Constructs protobuf inputlog objects in a RootMessage from the db
	 * 
	 * @param context
	 */
	public RootMessage getProtoInputLogObjects(Context context) {
		DataSource ds = new DataSource(context);
		ds.openRead();

		ArrayList<de.winteger.piap.core.InputLog> ls = new ArrayList<de.winteger.piap.core.InputLog>();
		ds.getAllLogEntries(ls);

		RootMessage.Builder b = RootMessage.newBuilder();

		for (de.winteger.piap.core.InputLog il : ls) {
			de.winteger.piap.proto.Messages.InputLog.Builder log = de.winteger.piap.proto.Messages.InputLog.newBuilder();

			log.setApp(il.getAppPkg());
			log.setTimestamp(il.getTimestamp());
			log.setText(il.getInput());

			b.addInputLog(log.build());

		}

		ds.close();
		return b.build();

	}

	/**
	 * Constructs protobuf mood objects in a RootMessage from the last
	 * evaluation
	 * 
	 * @param context
	 *            The context
	 * @return The RootMessage with mood objects
	 */
	public RootMessage getProtoMoodLogObjects(Context context) {
		Evaluation eval = null;
		eval = EvalAccess.loadLastEvaluation(context);

		if (eval == null) {
			// TODO: Throw no eval exception
			return null;
		}

		HashMap<Date, int[]> map = new HashMap<Date, int[]>();
		ArrayList<Date> keys = new ArrayList<Date>();
		eval.computeDailyData(map, keys);
		return getProtoMoodLogObjects(context, map);
	}

	/**
	 * Constructs protobuf mood objects in a RootMessage from the given context
	 * and map
	 * 
	 * @param context
	 *            The context
	 * @param map
	 *            The map of dates and category counters for each category
	 * @return The RootMessage with mood objects
	 */
	public RootMessage getProtoMoodLogObjects(Context context, HashMap<Date, int[]> map) {
		// TODO: map == null?

		RootMessage.Builder rmb = RootMessage.newBuilder();

		Set<Entry<Date, int[]>> set = map.entrySet();
		List<Entry<Date, int[]>> l = new ArrayList<Entry<Date, int[]>>(set);

		// sort list
		Collections.sort(l, new EntryComparator<Object>());

		// category descriptors
		String[] firstTitle = { context.getString(R.string.chart_total) };
		String[] titles = GlobalSettings.concat(firstTitle, EvalAccess.getCategoryDescriptors(context));
		int[] firstColor = { Color.BLACK };

		for (Entry<Date, int[]> e : l) {
			if (e == null || e.getValue() == null || e.getKey() == null) {
				continue;
			}

			int[] catCounts = e.getValue();
			for (int i = 0; i < catCounts.length; i++) {
				if (catCounts[i] > 0) {
					MoodLog.Builder mb = MoodLog.newBuilder();
					mb.setCategoryId(i);
					mb.setCategory(titles[i]);
					mb.setCount(catCounts[i]);
					mb.setTimestamp(e.getKey().getTime());
					rmb.addMoodLog(mb.build());
				}
			}

		}

		return rmb.build();
	}

	/**
	 * Compares Map.Entry<Date,V> by the time of the date
	 * 
	 * @author paul
	 * 
	 * @param <E>
	 */
	class EntryComparator<E> implements Comparator {

		@Override
		public int compare(Object lhs, Object rhs) {
			Date dlhs = null;
			Date drhs = null;

			if (lhs instanceof Entry) {
				if (((Entry) lhs).getKey() instanceof Date) {
					dlhs = (Date) ((Entry) lhs).getKey();
				}
			}

			if (rhs instanceof Entry) {
				if (((Entry) rhs).getKey() instanceof Date) {
					drhs = (Date) ((Entry) rhs).getKey();
				}
			}

			if (dlhs != null && drhs != null) {
				DateComperator d = new DateComperator();
				return d.compare(dlhs, dlhs);
			}

			return 0;
		}

	}
}
