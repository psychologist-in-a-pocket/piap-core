package de.winteger.piap.guicontent;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import de.winteger.piap.R;
import de.winteger.piap.core.GlobalSettings;
import de.winteger.piap.evaluation.EvaluationScheduleManager;
import de.winteger.piap.gui.SelectAppsActivity;
import de.winteger.piap.helper.DeviceInfo;

/**
 * Activity to view and edit the settings for the app
 * 
 * @author sarah
 * 
 */
public class ContentSettings extends AContentItem {
	public ContentSettings(String id, String title, int iconID) {
		super(id, title, iconID);
	}

	private static final String TAG = "Logger";

	private Context ctx;
	private CheckBox cBLogger;
	private CheckBox cBCharging;
	private CheckBox cBLocationLogging;
	private CheckBox cBNotify;
	private RelativeLayout rlLogging;
	private RelativeLayout rlApplications;
	private RelativeLayout rlInterval;
	private RelativeLayout rlCharging;
	private RelativeLayout rlLocationLogging;
	private RelativeLayout rlHomeLocation;
	private RelativeLayout rlNotify;
	private RelativeLayout rlContacts;
	private TextView tvInterval;
	private final int ACTIVITY_RESULT_LOGGING_SETTINGS = 17;
	private final int ACTIVITY_RESULT_LOCATION_SETTINGS = 18;

	private View rootView;

	private void disableNotificationSettings() {
		rootView.findViewById(R.id.tvPersonalSettings).setEnabled(false);
		rootView.findViewById(R.id.tvNotify).setEnabled(false);
		rootView.findViewById(R.id.tvNotifyDescriptor).setEnabled(false);
		rootView.findViewById(R.id.tvContacts).setEnabled(false);
		rootView.findViewById(R.id.tvContactsDescriptor).setEnabled(false);
		cBNotify.setEnabled(false);
	}

	private void disableLocationLoggingSettings() {
		rootView.findViewById(R.id.tvLocationSettings).setEnabled(false);
		rootView.findViewById(R.id.tvLocationLogging).setEnabled(false);
		rootView.findViewById(R.id.tvLocationLoggingDescriptor).setEnabled(false);
		rootView.findViewById(R.id.tvHomeLocation).setEnabled(false);
		rootView.findViewById(R.id.tvHomeLocationDescriptor).setEnabled(false);
		cBLocationLogging.setEnabled(false);
	}

	/**
	 * Returns nice description of the evaluation interval
	 * 
	 * @param storedInterval
	 *            the interval
	 * @return nice description
	 */
	private String IntervalSettingsToText(int storedInterval) {
		if (storedInterval == 0) {
			return "Never";
		} else if (storedInterval == 1) {
			return storedInterval + ctx.getString(R.string._hour);
		} else {
			return storedInterval + ctx.getString(R.string._hours);
		}
	}

	/**
	 * Checks if the Logger AccessibilityService is enabled
	 * 
	 * @return true if logging is enables
	 */
	private boolean isLoggerAccessibilityEnabled() {
		int accessibilityEnabled = 0;
		final String MY_SERVICE = ctx.getPackageName() + "/" + ctx.getPackageName() + ".logservice.LoggerService";
		Log.d(TAG, "Search for " + MY_SERVICE);
		boolean accessibilityFound = false;
		try {
			accessibilityEnabled = Settings.Secure.getInt(ctx.getContentResolver(), android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
			Log.d(TAG, "ACCESSIBILITY: " + accessibilityEnabled);
		} catch (SettingNotFoundException e) {
			Log.d(TAG, "Error finding setting, default accessibility not found: " + e.getMessage());
		}

		TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

		if (accessibilityEnabled == 1) {
			Log.d(TAG, "***ACCESSIBILIY IS ENABLED***: ");

			String settingValue = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
			Log.d(TAG, "Setting: " + settingValue);
			if (settingValue != null) {
				TextUtils.SimpleStringSplitter splitter = mStringColonSplitter;
				splitter.setString(settingValue);
				while (splitter.hasNext()) {
					String accessabilityService = splitter.next();
					Log.d(TAG, "Setting: " + accessabilityService);
					if (accessabilityService.equalsIgnoreCase(MY_SERVICE)) {
						Log.d(TAG, "We've found the correct setting - accessibility is switched on!");
						return true;
					}
				}
			}
			Log.d(TAG, "***END***");
		} else {
			Log.d(TAG, "***ACCESSIBILIY IS DISABLED***");
		}
		return accessibilityFound;
	}

	/**
	 * Opens the Accessibility Service Settings
	 */
	private void openLoggerSettings() {
		// geht leider nicht, weil WRITE_SECURE_SETTINGS is a signatureOrSystem permission
		//		int accessibilityEnabled = 0;
		//		try {
		//			accessibilityEnabled = Settings.Secure.getInt(this.getContentResolver(), android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
		//			Log.d("Logger", "ACCESSIBILITY: " + accessibilityEnabled);
		//		} catch (SettingNotFoundException e) {
		//			Log.d("Logger", "Error finding setting, default accessibility to not found: " + e.getMessage());
		//		}
		//		accessibilityEnabled = accessibilityEnabled == 0 ? 1 : 0;
		//		Settings.Secure.putInt(this.getContentResolver(), android.provider.Settings.Secure.ACCESSIBILITY_ENABLED, accessibilityEnabled);
		//		isLoggerAccessibilityEnabled();
		CManager.ACT.startActivityForResult(new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS), ACTIVITY_RESULT_LOGGING_SETTINGS);
	}

	/**
	 * is called when the Accessibility Service Settings returns updates the gui
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// not needed, as this function (here) is called recursively
		//super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		// we returned from en- or disabling the Text logging
		case ACTIVITY_RESULT_LOGGING_SETTINGS:
			Log.i(TAG, "resCode canceled " + (resultCode == Activity.RESULT_CANCELED));

			cBLogger.setChecked(isLoggerAccessibilityEnabled());
			break;
		// we returned from setting the home location
		case ACTIVITY_RESULT_LOCATION_SETTINGS:
			Log.i(TAG, "resCode canceled " + (resultCode == Activity.RESULT_CANCELED));
			// if location logging is enabled, update proximity alert to new location
			// if resCode = result ok, the save button was clicked
			if (resultCode == Activity.RESULT_OK && GlobalSettings.getLocationLoggingSetting()) {
				GlobalSettings.updateLocationLogging(CManager.CTX.getApplicationContext(), true);
			}
			break;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ctx = CManager.CTX;
		rootView = inflater.inflate(R.layout.activity_settings, container, false);

		cBLogger = (CheckBox) rootView.findViewById(R.id.cBLogging);
		cBLogger.setChecked(isLoggerAccessibilityEnabled());

		cBCharging = (CheckBox) rootView.findViewById(R.id.cBCharging);
		cBCharging.setChecked(GlobalSettings.getChargingSetting());
		cBLocationLogging = (CheckBox) rootView.findViewById(R.id.cBLocationLogging);
		cBLocationLogging.setChecked(GlobalSettings.getLocationLoggingSetting());
		cBNotify = (CheckBox) rootView.findViewById(R.id.cBNotify);
		cBNotify.setChecked(GlobalSettings.getNotifySetting());

		tvInterval = (TextView) rootView.findViewById(R.id.tvAutomaticEvalDescriptor);
		tvInterval.setText(IntervalSettingsToText(GlobalSettings.getIntervalHours()));

		rlLogging = (RelativeLayout) rootView.findViewById(R.id.rlLogging);
		rlLogging.setClickable(true);
		rlLogging.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// click on Logging opens the Accessibility-Service Settings
				openLoggerSettings();

			}
		});

		rlApplications = (RelativeLayout) rootView.findViewById(R.id.rlApplications);
		rlApplications.setClickable(true);
		rlApplications.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// click on Applications opens the SelectAppsActivity
				Intent intentSelectApps = new Intent(ctx, SelectAppsActivity.class);
				ctx.startActivity(intentSelectApps);
			}
		});

		rlInterval = (RelativeLayout) rootView.findViewById(R.id.rlInterval);
		rlInterval.setClickable(true);
		rlInterval.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// click on Automatic Evaluation Interval opens DialogBox with Radio-Buttons to select the interval
				LayoutInflater inflater = LayoutInflater.from(ctx);
				final View addView;

				addView = inflater.inflate(R.layout.dialog_interval, null);

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
				alertDialog.setView(addView);
				alertDialog.setPositiveButton(ctx.getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// read out which radioButton is checked
						RadioGroup rg = (RadioGroup) addView.findViewById(R.id.radioGroupInterval);
						int radioChecked = rg.getCheckedRadioButtonId();
						int interval = 0;
						switch (radioChecked) {
						case R.id.radio0h:
							interval = 0;
							break;
						case R.id.radio1h:
							interval = 1;
							break;
						case R.id.radio2h:
							interval = 2;
							break;
						case R.id.radio3h:
							interval = 3;
							break;
						case R.id.radio6h:
							interval = 6;
							break;
						case R.id.radio12h:
							interval = 12;
							break;
						case R.id.radio24h:
							interval = 24;
							break;
						}
						// set up interval accordingly
						GlobalSettings.setIntervalHours(interval);
						EvaluationScheduleManager m = new EvaluationScheduleManager();
						m.updateSchedule(ctx);
						// display new interval in gui
						tvInterval.setText(IntervalSettingsToText(GlobalSettings.getIntervalHours()));
					}
				});
				alertDialog.setNegativeButton(ctx.getString(R.string.cancel), null);
				alertDialog.setTitle(ctx.getString(R.string.select_interval));
				alertDialog.show();
			}
		});

		rlCharging = (RelativeLayout) rootView.findViewById(R.id.rlCharging);
		rlCharging.setClickable(true);
		rlCharging.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// click on Only When Charging toggles the setting
				boolean toggle = GlobalSettings.getChargingSetting() ? false : true;
				GlobalSettings.setChargingSetting(toggle);
				cBCharging.setChecked(GlobalSettings.getChargingSetting());
			}
		});

		final boolean pluginLocationInstalled = DeviceInfo.isInstalled(ctx, GlobalSettings.PIAP_PLUGIN_LOCATION_PKG);

		rlLocationLogging = (RelativeLayout) rootView.findViewById(R.id.rlLocationLogging);
		rlLocationLogging.setClickable(true);
		rlLocationLogging.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				if (!pluginLocationInstalled) {
					Toast.makeText(ctx, ctx.getString(R.string.to_use_this_feature_install_psychologist_in_a_pocket_location_module), Toast.LENGTH_LONG).show();
				} else {

					// click on Notify Contacts toggles the setting
					boolean toggle = GlobalSettings.getLocationLoggingSetting() ? false : true;
					GlobalSettings.setLocationLoggingSetting(toggle);
					cBLocationLogging.setChecked(GlobalSettings.getLocationLoggingSetting());
					// update proximity alert
					GlobalSettings.updateLocationLogging(ctx, GlobalSettings.getLocationLoggingSetting());
				}
			}
		});

		rlHomeLocation = (RelativeLayout) rootView.findViewById(R.id.rlHomeLocation);
		rlHomeLocation.setClickable(true);
		rlHomeLocation.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				Intent intentHomeLocation = new Intent();
				intentHomeLocation.setComponent(new ComponentName(GlobalSettings.PIAP_PLUGIN_LOCATION_PKG, GlobalSettings.PIAP_PLUGIN_LOCATION_PKG
						+ ".LocationSettingsActivity"));

				// Notification if plugin is not installed
				if (!pluginLocationInstalled) {
					Toast.makeText(ctx, ctx.getString(R.string.to_use_this_feature_install_psychologist_in_a_pocket_location_module), Toast.LENGTH_LONG).show();
				} else {
					// These flags can not be used with startActivityForResult correctly
					//intentHomeLocation.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					//intentHomeLocation.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
					CManager.ACT.startActivityForResult(intentHomeLocation, ACTIVITY_RESULT_LOCATION_SETTINGS);
				}
			}
		});

		// gray it out
		if (!pluginLocationInstalled) {
			disableLocationLoggingSettings();
		}

		final boolean pluginNotifierInstalled = DeviceInfo.isInstalled(ctx, GlobalSettings.PIAP_PLUGIN_NOTIFIER_PKG);

		rlNotify = (RelativeLayout) rootView.findViewById(R.id.rlNotify);
		rlNotify.setClickable(true);
		rlNotify.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// Notification if plugin is not installed
				if (!pluginNotifierInstalled) {
					Toast.makeText(ctx, ctx.getString(R.string.to_use_this_feature_install_psychologist_in_a_pocket_notifier), Toast.LENGTH_LONG).show();
				} else {
					// click on Notify Contacts toggles the setting
					boolean toggle = GlobalSettings.getNotifySetting() ? false : true;
					GlobalSettings.setNotifySetting(toggle);
					cBNotify.setChecked(GlobalSettings.getNotifySetting());
				}
			}
		});

		rlContacts = (RelativeLayout) rootView.findViewById(R.id.rlContacts);
		rlContacts.setClickable(true);
		rlContacts.setOnClickListener(new View.OnClickListener() {

			@SuppressLint("InlinedApi")
			public void onClick(View v) {
				// Notify broadcast receiver, receiver will start contact picker GUI
				Intent intent = new Intent(GlobalSettings.NOTIFIER_MANAGE_CONTACTS);

				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR1) {
					intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
				}

				ctx.sendBroadcast(intent);

				// Notification if plugin is not installed
				if (!pluginNotifierInstalled) {
					Toast.makeText(ctx, ctx.getString(R.string.to_use_this_feature_install_psychologist_in_a_pocket_notifier), Toast.LENGTH_LONG).show();
				}

			}

		});

		if (!pluginNotifierInstalled) {
			disableNotificationSettings();
		}

		return rootView;
	}

}
