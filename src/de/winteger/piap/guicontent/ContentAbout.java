package de.winteger.piap.guicontent;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;
import de.winteger.piap.R;
import de.winteger.piap.core.GlobalSettings;
import de.winteger.piap.evaluation.EvaluationScheduleManager;

/**
 * This activity displays the about infos for the app
 * 
 * @author sarah
 * 
 */
public class ContentAbout extends AContentItem {

	private Context ctx;

	private View.OnClickListener oclEnableDemo = new OnClickListener() {
		final int clicksToEnableDemo = 7;
		int numClicks = 0;

		@Override
		public void onClick(View v) {
			numClicks++;
			if (numClicks == clicksToEnableDemo) {
				GlobalSettings.setDemo(true);
				Toast.makeText(CManager.CTX, "Enabled DEMO mode", Toast.LENGTH_LONG).show();
				EvaluationScheduleManager.updateSchedule(CManager.CTX);
			}
		}
	};

	public ContentAbout(String id, String title, int iconID) {
		super(id, title, iconID);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ctx = CManager.CTX;
		View rootView = inflater.inflate(R.layout.activity_about, container, false);
		TextView tvAbout = (TextView) rootView.findViewById(R.id.tvAbout0);
		WebView wv = (WebView) rootView.findViewById(R.id.wvLicences);
		MyWebViewClient mc = new MyWebViewClient();

		tvAbout.setOnClickListener(oclEnableDemo);
		wv.setWebViewClient(mc);
		wv.loadUrl("file:///android_asset/about.html");

		return rootView;
	}

	class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(url));
			CManager.CTX.startActivity(i);

			return true;
		}
	}

}