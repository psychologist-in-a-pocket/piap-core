package de.winteger.piap.guicontent;

import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import de.winteger.piap.R;
import de.winteger.piap.core.Plugin;
import de.winteger.piap.listadapter.ArrayAdapterPlugins;

public class ContentPlugins extends AContentItem {

	private ArrayAdapterPlugins aap;

	private ListView lvPlugins = null;

	private List<Plugin> pluginList;

	public ContentPlugins(String id, String title, int iconID) {
		super(id, title, iconID);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_plugin, container, false);
		lvPlugins = (ListView) rootView.findViewById(R.id.lvPlugins);
		LayoutInflater myInflater = (LayoutInflater) CManager.CTX.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		pluginList = CManager.getPlugins();

		aap = new ArrayAdapterPlugins(CManager.CTX, myInflater, R.id.lvDB, pluginList);
		lvPlugins.setAdapter(aap);
		return rootView;
	}
}
