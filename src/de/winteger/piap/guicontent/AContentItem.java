package de.winteger.piap.guicontent;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.winteger.piap.R;

public abstract class AContentItem {
	public String ID;
	public String title = null;
	protected int iconID = R.drawable.ic_launcher;
	
	
	
	public AContentItem(String id, String title) {
		this.ID = id;
		this.title = title;
	}

	public AContentItem(String id, String title, int iconID) {
		this.ID = id;
		this.title = title;
		this.iconID = iconID;
	}
	
	public View onCreateListItemView(LayoutInflater inflater, View convertView, ViewGroup parent) {
		View rootView = inflater.inflate(R.layout.md_content_item_simple, parent, false);
		ImageView ivIcon = (ImageView) rootView.findViewById(R.id.ivListIcon);
		ivIcon.setImageResource(iconID);
		TextView tvLabel = (TextView) rootView.findViewById(R.id.tvLabel);
		tvLabel.setText(title);
		return rootView;
	}

	public abstract View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

	public void onActivityResult(int requestCode, int resultCode, Intent data) {

	}

	public void onSaveInstanceState(Bundle outState) {

	}

	public void onPause() {

	}

	public void onResume() {

	}

	public void onDestroy() {

	}

	public boolean onCreateOptionsMenu(Menu menu, MenuInflater inflater){
		return false;
	}

	public boolean onOptionsItemSelected(MenuItem item){
		return false;
	}

	@Override
	public String toString() {
		return title;
	}
}
