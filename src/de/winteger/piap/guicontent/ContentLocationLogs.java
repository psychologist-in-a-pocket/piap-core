package de.winteger.piap.guicontent;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.winteger.piap.R;
import de.winteger.piap.core.LocationLog;
import de.winteger.piap.data.DataSource;
import de.winteger.piap.helper.TimeHelper;
import de.winteger.piap.listadapter.ArrayAdapterLocationLogs;

/**
 * This activity displays the location logs, allows the user to delete and
 * export entries
 * 
 * @author sarah
 * 
 */
public class ContentLocationLogs extends AContentItem {

	private Context ctx;

	private static final String TAG = "Logger";
	private DataSource datasource; // DB access object
	private ArrayList<LocationLog> dbEntryList; // used to store the logs
	private ArrayAdapterLocationLogs dbArrayAdapter; // notifies the view
	private ListView dbListView; // displays the logs

	public ContentLocationLogs(String id, String title, int iconID) {
		super(id, title, iconID);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ctx = CManager.CTX;
		View rootView = inflater.inflate(R.layout.activity_view_location_logs, container, false);

		dbListView = (ListView) rootView.findViewById(R.id.lvLocationLogs);
		dbEntryList = new ArrayList<LocationLog>();

		// get all logs from the DB
		datasource = new DataSource(ctx);
		datasource.openWrite();
		datasource.getAllLocationEntries(dbEntryList, 0, System.currentTimeMillis());
		datasource.close();

		LayoutInflater myInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// set up the view
		dbArrayAdapter = new ArrayAdapterLocationLogs(ctx, myInflater, R.id.lvLocationLogs, dbEntryList);
		dbListView.setAdapter(dbArrayAdapter);
		dbListView.setOnItemClickListener(defaultListOnItemOnClickHandler);
		dbListView.setOnItemLongClickListener(defaultListOnItemLongClickHandler);

		return rootView;
	}

	private OnItemClickListener defaultListOnItemOnClickHandler = new OnItemClickListener() {

		//@Override
		public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
			// no special function for clicks on the list items
			Log.i(TAG, "Clicked on pos(" + pos + ") id(" + id + ")");
		}
	};

	private OnItemLongClickListener defaultListOnItemLongClickHandler = new OnItemLongClickListener() {

		//@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View view, int pos, long id) {
			// Log can be deleted from the DB on LongClick
			Log.i(TAG, "LongClicked on pos(" + pos + ") id(" + id + ")");

			final LocationLog temp = dbEntryList.get(pos);

			AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);

			alertDialog.setPositiveButton(ctx.getString(R.string.delete), new DialogInterface.OnClickListener() {
				//@Override
				public void onClick(DialogInterface dialog, int which) {
					// delete log, notify gui
					datasource.openWrite();
					datasource.deleteLocationEntry(temp);
					datasource.getAllLocationEntries(dbEntryList, 0, System.currentTimeMillis());
					datasource.close();

					dbArrayAdapter.notifyDataSetChanged();
				}
			});

			alertDialog.setNegativeButton(ctx.getString(R.string.cancel), null);
			String strx = temp.isEntering() ? "Returned on " : "Left on ";
			alertDialog.setMessage(strx + TimeHelper.getDateTime(CManager.CTX, temp.getTimestamp()));
			alertDialog.setTitle(ctx.getString(R.string.delete_the_entry));
			alertDialog.show();

			return false;
		}

	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO: delete last entry from this menu...
		inflater.inflate(R.menu.activity_view_location, menu);
		return true;
	}

	/**
	 * Handles the menu clicks
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_delete_all_logs:
			// delete all logs from the DB, notify view
			new AlertDialog.Builder(ctx).setTitle(ctx.getString(R.string.delete_all_logs))
					.setNegativeButton(ctx.getString(R.string.cancel), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							// ignore, just dismiss
						}
					}).setPositiveButton(ctx.getString(R.string.ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							datasource.openWrite();
							datasource.deleteAllLocationLogs();
							datasource.getAllLocationEntries(dbEntryList, 0, System.currentTimeMillis());
							datasource.close();

							dbArrayAdapter.notifyDataSetChanged();
						}
					}).setMessage(ctx.getString(R.string.this_will_delete_all_logs)).show();
			break;
		case R.id.menu_export_logs:
			// export logs to external storage
			new AlertDialog.Builder(ctx).setTitle(ctx.getString(R.string.export_logs))
					.setPositiveButton(ctx.getString(R.string.export), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							// TODO: maybe in a thread? export date instead of timestamp..
							try {
								// creates the file (directory) if it does not already exist...
								File parentDirectory = new File(getExternalStorageDir() + "/PsychologistInAPocket/", "/LocationLogs/");
								if (!parentDirectory.exists()) {
									Log.i(TAG, "It seems like the parent directory does not exist...");
									if (!parentDirectory.mkdirs()) {
										Log.i(TAG, "And we cannot create it...");
										// we have to return, throw or something else
									}
								}
								String fileName = System.currentTimeMillis() + "locations.txt"; // filename to export to
								File logFile = new File(parentDirectory, fileName);
								// write logs to file
								BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
								for (LocationLog log : dbEntryList) {
									String msg = "";
									// TODO: internationalize this
									if (log.isEntering()) {
										msg = "Entered home zone.";
									} else {
										msg = "Left home zone.";
									}
									buf.write(TimeHelper.getDateTime(ctx, log.getTimestamp()) + ";" + msg + "\n");
								}
								buf.flush();
								buf.close();
								shortToast(ctx.getString(R.string.exported_logs_to) + logFile.getPath());
							} catch (FileNotFoundException e) {
								Log.i(TAG, "Could not save, file was not found" + e.getMessage());
							} catch (IOException e) {
								Log.i(TAG, "io exception" + e.getMessage());
							}
						}
					}).setNegativeButton(ctx.getString(R.string.cancel), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							// ignore, just dismiss
						}
					}).setMessage(ctx.getString(R.string.dialog_info_export_logs)).show();
			break;
		case R.id.sort_logs_time_ascending:
			// Sort logs by time, ascending
			sortLogsByTimeAscending();
			break;
		case R.id.sort_logs_time_descending:
			// Sort logs by time, descending
			sortLogsByTimeDescending();
			break;
		default:
			break;
		}
		return true;
	}

	/**
	 * Return the directory of the external storage
	 * 
	 * @return the directory
	 */
	public File getExternalStorageDir() {
		File extStore = Environment.getExternalStorageDirectory();
		return extStore;
	}

	/**
	 * Displays a short toast with message msg
	 * 
	 * @param msg
	 */
	private void shortToast(String msg) {

		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(ctx, msg, duration);
		toast.show();
	}

	/**
	 * sorts logs ascending by time
	 */
	private void sortLogsByTime() {
		Collections.sort(dbEntryList, new Comparator<LocationLog>() {
			public int compare(LocationLog l0, LocationLog l1) {
				// an integer < 0 if lhs is less than rhs
				// 0 if they are equal
				// and > 0 if lhs is greater than rhs.
				if (l0.getTimestamp() == l1.getTimestamp()) {
					return 0;
				} else if (l0.getTimestamp() < l1.getTimestamp()) {
					return -1;
				} else {
					return 1;
				}
			}
		});
	}

	/**
	 * sorts logs ascending by time, notifies the view
	 */
	private void sortLogsByTimeAscending() {
		sortLogsByTime();
		dbArrayAdapter.notifyDataSetChanged();
	}

	/**
	 * sorts logs descending by time, notifies the view
	 */
	private void sortLogsByTimeDescending() {
		sortLogsByTime();
		Collections.reverse(dbEntryList);
		dbArrayAdapter.notifyDataSetChanged();
	}

}
