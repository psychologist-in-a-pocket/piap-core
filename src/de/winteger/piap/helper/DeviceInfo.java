package de.winteger.piap.helper;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.BatteryManager;
import de.winteger.piap.R;

public class DeviceInfo {

	/**
	 * Checks if the device is charging and/or is connected to an external power
	 * source
	 * 
	 * @param context
	 *            The context in which the Receiver is running
	 * @return true if the device is connected to an external power source
	 */
	public static boolean hasExternalPower(Context context) {
		boolean charging = false;
	
		// Listen for the battery sticky broadcast
		final Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		// Get the current charging status
		// Note: If the battery is 100%, the device is not in status 'charging'
		int status = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
		// Set boolean flag
		boolean batteryCharge = (status == BatteryManager.BATTERY_STATUS_CHARGING);
	
		// Get the current 'plugged-in' status
		int chargePlug = batteryIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
		// Set boolean flags
		boolean usbCharge = (chargePlug == BatteryManager.BATTERY_PLUGGED_USB);
		boolean acCharge = (chargePlug == BatteryManager.BATTERY_PLUGGED_AC);
		// only for API Level 17 (4.2)
		// boolean wirelessCharge = (chargePlug == BatteryManager.BATTERY_PLUGGED_WIRELESS);
	
		// if the battery is charging and/or the device is connected to an external power source set charging to true
		if (batteryCharge || usbCharge || acCharge) {
			charging = true;
		}
	
		return charging;
	}

	/**
	 * Checks whether a package with the given package name is installed
	 * 
	 * @param context
	 *            The context
	 * @param pkg
	 *            The package name
	 * @return
	 */
	public static boolean isInstalled(Context context, String pkg) {
		try {
			// this is faster than retreiving all installed packages and 
			// then looking if it is in the list of installed packages
			@SuppressWarnings("unused")
			ApplicationInfo info = context.getPackageManager().getApplicationInfo(pkg, 0);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

	/**
	 * Tries to install the given package from the play store.
	 * 
	 * @param context
	 * @param pkg
	 */
	public static void installPackage(Context context, String pkg) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("market://details?id=" + pkg));
		context.startActivity(intent);
	}

	public static int getAplicationIcon(Context ctx, String pkg) {
		PackageManager pm = ctx.getPackageManager();
		ApplicationInfo ai;
		try {
			ai = pm.getApplicationInfo(pkg, 0);
		} catch (final NameNotFoundException e) {
			ai = null;
		}
	
		return ai != null ? ai.icon : R.drawable.ic_launcher;
	}

	public static String getAplicationName(Context ctx, String pkg) {
		PackageManager pm = ctx.getPackageManager();
		ApplicationInfo ai;
		try {
			ai = pm.getApplicationInfo(pkg, 0);
		} catch (final NameNotFoundException e) {
			ai = null;
		}
		return ai != null ? pm.getApplicationLabel(ai).toString() : "N/A";
	}

}
