package de.winteger.piap.core;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.widget.Toast;
import de.winteger.piap.R;
import de.winteger.piap.evaluation.Evaluation;
import de.winteger.piap.guicontent.CManager;

/**
 * Class provides access to the global application settings
 * 
 * @author sarah
 * 
 */
public class GlobalSettings {

	static final String TAG = "Logger";

	// Packagenames for plugins
	public static final String PIAP_PLUGIN_NOTIFIER_PKG = "de.winteger.piap.plugin.notifier";
	public static final String PIAP_PLUGIN_LOCATION_PKG = "de.winteger.piap.plugin.location";
	public static final String PIAP_PLUGIN_SENDTOOHMAGE_PKG = "de.winteger.piap.plugin.ohmage";
	public static final String PIAP_PLUGIN_SENDTODEVICE_PKG = "de.winteger.piap.plugin.sendtodevice";
	public static final String PIAP_APP_OHMAGE_CLIENT_PKG = "org.ohmage.app";

	// Strings for settings
	private static final String INTERVAL_TIME_IN_HOURS = "intervallTimeInHours";
	private static final String EVALUATE_ONLY_WHEN_CHARGING = "evalOnlyWhenCharging";
	private static final String LOG_LOCATION = "logLocation";
	private static final String NOTIFY_CONTACTS = "notifyContacs";
	private static final String DEMO_MODE = "demo";
	private static final String EVALUATION_RESULT = "evalResult";
	private static final String LAST_SMS_TIMESTAMP = "smsTimeStamp";
	private static final String LAST_EVAL_TIMESTAMP = "evalTimeStamp";
	private static final String PREF_ID = "MyPreferences";

	// const for unset sms timestamp value
	public static final long LAST_SMS_TIMESTAMP_UNSET = -1;
	public static final long LAST_EVAL_TIMESTAMP_UNSET = -1;

	// Fields for evaluation modes
	public static final String EVAL_MODE = "mode";
	public static final int EVAL_MODE_USER = 123;
	public static final int EVAL_MODE_SCHEDULE = 456;
	public static final int EVAL_MODE_ON_AC_CONNECT = 789;

	// Strings for intent actions
	public static final String LOCATION_ALERT = "de.winteger.piap.plugin.location.LOCATION_ALERT";
	public static final String LOCATION_ALERT_ADD = "de.winteger.piap.plugin.location.LOCATION_ALERT_ADD";
	public static final String LOCATION_ALERT_REMOVE = "de.winteger.piap.location.LOCATION_ALERT_REMOVE";
	public static final String NOTIFIER_MANAGE_CONTACTS = "de.winteger.piap.plugin.notifier.MANAGE_CONTACTS";
	public static final String NOTIFIER_SEND_SMS = "de.winteger.piap.plugin.notifier.SEND_SMS";
	public static final String PIAP_SHOWEVAL = "de.winteger.piap.SHOWEVAL";

	public static final String PERMISSION_SEND_HEALTH_STATUS = "de.winteger.psych.permissions.SendPsychologicalHealthStatus";

	private static boolean isValueSet(String id, boolean deflt) {
		SharedPreferences settings = CManager.CTX.getSharedPreferences(PREF_ID, 0);
		boolean res = settings.getBoolean(id, deflt);
		return res;
	}

	private static void setValue(String id, boolean value) {
		SharedPreferences settings = CManager.CTX.getSharedPreferences(PREF_ID, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(id, value);
		editor.commit();
	}

	private static int getValueInt(String id, int deflt) {
		SharedPreferences settings = CManager.CTX.getSharedPreferences(PREF_ID, 0);
		int res = settings.getInt(id, deflt);
		return res;
	}

	private static void setValueInt(String id, int value) {
		SharedPreferences settings = CManager.CTX.getSharedPreferences(PREF_ID, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(id, value);
		editor.commit();
	}

	private static long getValueLong(String id, long deflt) {
		SharedPreferences settings = CManager.CTX.getSharedPreferences(PREF_ID, 0);
		long res = settings.getLong(id, deflt);
		return res;
	}

	private static void setValueLong(String id, long value) {
		SharedPreferences settings = CManager.CTX.getSharedPreferences(PREF_ID, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(id, value);
		editor.commit();
	}

	public static boolean isDemo() {
		return isValueSet(DEMO_MODE, false);
	}

	public static void setDemo(boolean value) {
		setValue(DEMO_MODE, value);
	}

	public static int getIntervalHours() {
		return getValueInt(INTERVAL_TIME_IN_HOURS, 3);
	}

	public static void setIntervalHours(int hours) {
		setValueInt(INTERVAL_TIME_IN_HOURS, hours);
	}

	public static boolean getChargingSetting() {
		return isValueSet(EVALUATE_ONLY_WHEN_CHARGING, true);
	}

	public static void setChargingSetting(boolean value) {
		setValue(EVALUATE_ONLY_WHEN_CHARGING, value);
	}

	public static boolean getNotifySetting() {
		return isValueSet(NOTIFY_CONTACTS, false);
	}

	public static void setNotifySetting(boolean value) {
		setValue(NOTIFY_CONTACTS, value);
	}

	public static boolean getLocationLoggingSetting() {
		return isValueSet(LOG_LOCATION, false);
	}

	public static void setLocationLoggingSetting(boolean value) {
		setValue(LOG_LOCATION, value);
	}

	public static int getSavedEvalResult() {
		return getValueInt(EVALUATION_RESULT, Evaluation.EVAL_NO_RESULT);
	}

	public static void setEvaluationResult(int result) {
		setValueInt(EVALUATION_RESULT, result);
	}

	public static long getLastSMSTimestamp() {
		return getValueLong(LAST_SMS_TIMESTAMP, LAST_SMS_TIMESTAMP_UNSET);
	}

	public static void setLastSMSTimestamp(long timestamp) {
		setValueLong(LAST_SMS_TIMESTAMP, timestamp);
	}

	public static long getLastEvalTimestamp() {
		return getValueLong(LAST_EVAL_TIMESTAMP, LAST_EVAL_TIMESTAMP_UNSET);
	}

	public static void setLastEvalTimestamp(long timestamp) {
		setValueLong(LAST_EVAL_TIMESTAMP, timestamp);
	}

	@SuppressLint("InlinedApi")
	// because of the FLAG_INCLUDE_STOPPED_PACKAGES
	public static void updateLocationLogging(Context context, boolean value) {
		// Notify broadcast receiver, receiver will update or remove proximity alert
		Intent intent = new Intent();
		if (value == true) {
			intent.setAction(LOCATION_ALERT_ADD);
		} else {
			intent.setAction(LOCATION_ALERT_REMOVE);
		}

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR1) {
			intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
		}

		context.sendBroadcast(intent);
	}

	public static long getSMSMinInterval() {
		// TODO: this is hardcoded: RETURN 3 DAYS
		return 1000 * 60 * 60 * 24 * 3;
	}

	public static String[] concat(String[] first, String[] second) {
		String[] result = new String[first.length + second.length];

		System.arraycopy(first, 0, result, 0, first.length);
		System.arraycopy(second, 0, result, first.length, second.length);

		return result;
	}

	public static int[] concat(int[] first, int[] second) {
		int[] result = new int[first.length + second.length];

		System.arraycopy(first, 0, result, 0, first.length);
		System.arraycopy(second, 0, result, first.length, second.length);

		return result;
	}

	@SuppressLint("InlinedApi")
	// because of the FLAG_INCLUDE_STOPPED_PACKAGES
	public static void sendSMSNotification(String txt) {

		// Notify broadcast receiver, receiver will send sms
		Intent intent = new Intent(GlobalSettings.NOTIFIER_SEND_SMS);

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR1) {
			intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
		}

		intent.putExtra("message", txt);
		CManager.CTX.sendBroadcast(intent, GlobalSettings.PERMISSION_SEND_HEALTH_STATUS);

		// Notification
		PackageManager p = CManager.CTX.getPackageManager();
		List<ResolveInfo> list = p.queryBroadcastReceivers(intent, 0);
		//Log.i(TAG, "get my info");
		//for (ResolveInfo info : list) {
		//	Log.i(TAG, "Info: " + info.toString());
		//}

		if (list.size() == 0) {
			shortToast(CManager.CTX.getString(R.string.to_use_this_feature_install_psychologist_in_a_pocket_notifier));
		}
	}

	/**
	 * Displays a short toast with message msg
	 * 
	 * @param msg
	 */
	public static void shortToast(String msg) {

		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(CManager.CTX, msg, duration);
		toast.show();
	}

}
