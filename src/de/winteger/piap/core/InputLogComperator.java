package de.winteger.piap.core;

import java.util.Comparator;

/**
 * InputLog Comparator 
 * @author sarah
 *
 */
public class InputLogComperator implements Comparator<InputLog> {

	/**
	 * compares log timestamps, lhs < rhs iff lhs.timestamp < rhs.timestamp
	 */
	public int compare(InputLog l0, InputLog l1) {
		// an integer < 0 if lhs is less than rhs
		// 0 if they are equal
		// and > 0 if lhs is greater than rhs.
		if (l0.getTimestamp() == l1.getTimestamp()) {
			return 0;
		} else if (l0.getTimestamp() < l1.getTimestamp()) {
			return -1;
		} else {
			return 1;
		}
	}
}