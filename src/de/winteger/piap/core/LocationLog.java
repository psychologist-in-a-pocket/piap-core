package de.winteger.piap.core;

import java.io.Serializable;

/**
 * This class stores information about a location status
 * 
 * @author sarah
 *
 */
public class LocationLog implements Serializable{
	
	private static final long serialVersionUID = 7653214477817503164L;
	
	private long timestamp;
	private boolean entering;
	
	public LocationLog() {

	}
	
	public LocationLog(long timestamp, boolean entering) {
		this.timestamp = timestamp;
		this.entering = entering;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isEntering() {
		return entering;
	}

	public void setEntering(boolean entering) {
		this.entering = entering;
	}

}
