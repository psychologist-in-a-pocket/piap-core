package de.winteger.piap.core;

/**
 * Contains information about an application
 * human-readable name, package, logging status
 * @author sarah
 *
 */
public class AppListItem {

	private String appName;
	private String appPkg;
	private boolean logEnabled = false;

	public boolean isLogEnabled() {
		return logEnabled;
	}

	public void setLogEnabled(boolean logEnabled) {
		this.logEnabled = logEnabled;
	}

	public AppListItem() {
		super();
	}

	public AppListItem(String appName, String appPkg) {
		super();
		this.appName = appName;
		this.appPkg = appPkg;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppPkg() {
		return appPkg;
	}

	public void setAppPkg(String appPkg) {
		this.appPkg = appPkg;
	}

	/**
	 * two AppListItems are equal if their appName and appPkg are equal
	 */
	public boolean equals(Object rhs) {
		if (!(rhs instanceof AppListItem)) {
			return false;
		}
		AppListItem rhsx = (AppListItem) rhs;
		if (appName != null && rhsx.appName != null && appPkg != null && rhsx.appPkg != null) {
			return appName.equals(rhsx.appName) && appPkg.equals(rhsx.appPkg);
		}
		return false;
	}
}
