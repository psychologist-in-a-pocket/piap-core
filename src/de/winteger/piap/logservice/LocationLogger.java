package de.winteger.piap.logservice;

import de.winteger.piap.data.DataSource;
import de.winteger.piap.guicontent.CManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;

public class LocationLogger extends BroadcastReceiver {

	private static final String TAG = "LoggerLocation";

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "received proximity alert");
		
		if (CManager.CTX == null){
			CManager.CTX = context;
		}

		// did we enter or leave the home zone?
		boolean entering = intent.getBooleanExtra(LocationManager.KEY_PROXIMITY_ENTERING, false);
		// when?
		long now = System.currentTimeMillis();

		// make a test-toast
		//		Log.i(TAG, "entered home zone " + entering);
		//		Toast.makeText(context, "entered home zone " + entering, Toast.LENGTH_SHORT).show();

		// store it in the DB
		logLocation(context, now, entering);
	}

	/**
	 * inserts the last location update (entering home or leaving home zone)
	 * into the DB
	 */
	private void logLocation(Context context, long timestamp, boolean entering) {
		// Initialize DB access object
		DataSource datasource = new DataSource(context);
		// open DB
		datasource.openWrite();
		long id = datasource.createLocationEntry(timestamp, entering);
		// close DB
		datasource.close();
	}

}
