package de.winteger.piap.mastergui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.winteger.piap.R;
import de.winteger.piap.core.GlobalSettings;
import de.winteger.piap.guicontent.CManager;

/**
 * An activity representing a single content detail screen. This activity is
 * only used on handset devices. On tablet-size devices, item details are
 * presented side-by-side with a list of items in a {@link ListActivity} .
 * <p>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link DetailFragment}.
 */
public class DetailActivity extends SherlockFragmentActivity {

	private String ITEM_ID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.md_activity_detail);
		CManager.CTX = this;
		CManager.ACT = this;

		// this is to catch the activity used to show the evaluation.
		if (getIntent() != null && getIntent().getAction() != null && getIntent().getAction().equals(GlobalSettings.PIAP_SHOWEVAL)) {
			CManager.initListData();
			Bundle arguments = new Bundle();
			ITEM_ID = "idEvaluation";
			arguments.putString(DetailFragment.ARG_ITEM_ID, ITEM_ID);
			DetailFragment fragment = new DetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction().add(R.id.main_container, fragment).commit();
			return;
		}

		// This is used to catch the call from system settings
		if (getIntent() != null && getIntent().getAction() != null && getIntent().getAction().equals(Intent.ACTION_MAIN)) {
			CManager.initListData();
			Bundle arguments = new Bundle();
			ITEM_ID = "idSettings";
			arguments.putString(DetailFragment.ARG_ITEM_ID, ITEM_ID);
			DetailFragment fragment = new DetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction().add(R.id.main_container, fragment).commit();
			return;
		}

		// Show the Up button in the action bar.
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		// savedInstanceState is non-null when there is fragment state
		// saved from previous configurations of this activity
		// (e.g. when rotating the screen from portrait to landscape).
		// In this case, the fragment will automatically be re-added
		// to its container so we don't need to manually add it.
		// For more information, see the Fragments API guide at:
		//
		// http://developer.android.com/guide/components/fragments.html
		//
		if (savedInstanceState == null) {
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
			Bundle arguments = new Bundle();
			ITEM_ID = getIntent().getStringExtra(DetailFragment.ARG_ITEM_ID);
			arguments.putString(DetailFragment.ARG_ITEM_ID, ITEM_ID);
			DetailFragment fragment = new DetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction().add(R.id.main_container, fragment).commit();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (ITEM_ID != null && CManager.ITEM_MAP.get(ITEM_ID) != null) {
			CManager.ITEM_MAP.get(ITEM_ID).onSaveInstanceState(outState);
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onResume() {
		CManager.CTX = this;
		CManager.ACT = this;
		if (ITEM_ID != null && CManager.ITEM_MAP.get(ITEM_ID) != null) {
			CManager.ITEM_MAP.get(ITEM_ID).onResume();
		}
		super.onResume();
	}

	@Override
	protected void onPause() {
		if (ITEM_ID != null && CManager.ITEM_MAP.get(ITEM_ID) != null) {
			CManager.ITEM_MAP.get(ITEM_ID).onPause();
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		if (ITEM_ID != null && CManager.ITEM_MAP.get(ITEM_ID) != null) {
			CManager.ITEM_MAP.get(ITEM_ID).onDestroy();
		}
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpTo(this, new Intent(this, ListActivity.class));
			return true;
		}
		if (ITEM_ID != null && CManager.ITEM_MAP.get(ITEM_ID) != null) {
			return CManager.ITEM_MAP.get(ITEM_ID).onOptionsItemSelected(item);
		}
		return true;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		menu.clear();
		if (ITEM_ID != null && CManager.ITEM_MAP.get(ITEM_ID) != null) {
			CManager.ITEM_MAP.get(ITEM_ID).onCreateOptionsMenu(menu, inflater);
		}
		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (ITEM_ID != null && CManager.ITEM_MAP.get(ITEM_ID) != null) {
			CManager.ITEM_MAP.get(ITEM_ID).onActivityResult(requestCode, resultCode, data);
		}
	}
}
